import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';

export const FILTER_ALL = 'FILTER/ALL';
export const FILTER_ACTIVE = 'FILTER/ACTIVE';
export const FILTER_COMPLETE = 'FILTER/COMPLETE';

const Props = {
  onClearComplete: PropTypes.func.isRequired,
  onFilter: PropTypes.func.isRequired,
};

class Footer extends Component<Props> {
  render() {
    const { filter, count } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.count} >{count} items</Text>
        <View style={styles.filters}>
          <TouchableOpacity
            style={[styles.filter, filter === FILTER_ALL && styles.selected ]}
            onPress={() => this.props.onFilter(FILTER_ALL)}
          >
            <Text>All</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => this.props.onFilter(FILTER_ACTIVE)}
            style={[styles.filter, filter === FILTER_ACTIVE && styles.selected ]}>
            <Text>Active</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            onPress={() => this.props.onFilter(FILTER_COMPLETE)}
            style={[styles.filter, filter === FILTER_COMPLETE && styles.selected ]}>
            <Text>Complete</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => this.props.onClearComplete()} style={styles.clear}>
          <Text>Clear completed</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  filters: {
    flexDirection: 'row',
  },
  selected: {
    borderColor: 'rgba(175, 175, 17, .2)',
  },
  filter: {
    padding: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'transparent'
  },
  clear: {
    borderWidth: 1,
    borderColor: 'rgba(12, 110, 0, .5)',
    padding: 8,
    backgroundColor: '#c8ffc2',
    borderRadius: 4,
  },
  count: {
    marginRight: 5
  }
});

Footer.propTypes = {};
Footer.defaultProps = {};

export default Footer;
