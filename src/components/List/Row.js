import React, { Component } from 'react';
import { Text, View, StyleSheet, Switch, TouchableOpacity, Image } from 'react-native';
import trashIcon from '../../images/trash.png';

class Row extends Component {
  render() {
    const { text, key, complete } = this.props.item;
    return (
      <View key={key} style={styles.row}>
        <Switch
          value={complete}
          onValueChange={this.props.handleToggleComplete}
        />
        <View style={styles.textWrap}>
          <Text style={[styles.text, complete && styles.complete]}>{text}</Text>
        </View>
        <TouchableOpacity onPress={this.props.handleRemoveItem} style={styles.imageWrapper}>
          <Image style={styles.destroy} source={trashIcon} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginLeft: 15,
    marginBottom: 10,
  },
  toggle: {
    height: 14
  },
  textWrap: {
    flex: 1,
    marginHorizontal: 10,
  },
  complete: {
    textDecorationLine: 'line-through'
  },
  imageWrapper: {
    flex: .2
  },
  destroy: {
    width: 20,
    height: 20,
    // flex: 1,
    resizeMode: 'cover'
  },
  text: {
    fontSize: 20,
    lineHeight: 24,
    color: '#4D4D4D'
  }
});

Row.propTypes = {};
Row.defaultProps = {};

export default Row;
