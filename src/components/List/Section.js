import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

class Section extends Component {
  render() {
    const { title } = this.props.section;
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          {title}
        </Text>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    flex: 1,
    fontSize: 20,
    color: '#000',
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#CCC'
  }
});

Section.propTypes = {};
Section.defaultProps = {};

export default Section;
