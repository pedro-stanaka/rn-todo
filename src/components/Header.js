import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput, View, StyleSheet, TouchableOpacity, Text } from 'react-native';

class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <TouchableOpacity onPress={this.props.handleToggleComplete}>
          <Text style={styles.toggleIcon}>{String.fromCharCode(10003)}</Text>
        </TouchableOpacity>
        <TextInput
          placeholder="What needs to be done?"
          blurOnSubmit={false}
          style={styles.input}
          value={this.props.value}
          onChangeText={this.props.onChange}
          onSubmitEditing={this.props.onAddItem}
          />
        <TouchableOpacity onPress={this.props.onAddItem}>
          <Text style={styles.addIcon}>{String.fromCharCode(43)}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

Header.propTypes = {
  onChange: PropTypes.func.isRequired,
  onAddItem: PropTypes.func.isRequired,
  handleToggleComplete: PropTypes.func.isRequired,
};
Header.defaultProps = {};

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  toggleIcon: {
    fontSize: 26,
    color: '#38da0f',
  },
  addIcon: {
    fontSize: 26,
    color: '#85838c',
  },
  input: {
    flex: 1,
    height: 50,
    marginLeft: 16,
  }
});

export default Header;
