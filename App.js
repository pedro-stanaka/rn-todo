/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { AsyncStorage, Platform, SectionList, StyleSheet, View } from 'react-native';
import Header from './src/components/Header';
import Footer, { FILTER_ACTIVE, FILTER_ALL, FILTER_COMPLETE } from './src/components/Footer';
import Row from './src/components/List/Row';
import Section from './src/components/List/Section';

const STATE_KEY = 'appState';

const filterItems = (items, filter) => {
  return items.filter(item => {
    if (filter === FILTER_ALL) return true;
    if (filter === FILTER_ACTIVE && !item.complete) return true;
    if (filter === FILTER_COMPLETE && item.complete) return true;
  });
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      items: [],
      filter: FILTER_ALL,
      showItems: []
    };

    this.handleAddItem = this.handleAddItem.bind(this);
    this.handleToggleAllComplete = this.handleToggleAllComplete.bind(this);
    this.handleToggleComplete = this.handleToggleComplete.bind(this);
    this.handleRemoveItem = this.handleRemoveItem.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleClearComplete = this.handleClearComplete.bind(this);
  }

  componentWillMount() {
    AsyncStorage.getItem(STATE_KEY).then(json => {
      try {
        console.log(json);
        const { items, filter } = JSON.parse(json);
        console.log(filter);
        this.setItems(items, filterItems(items, filter), { filter });
      } catch (e) {
      }
    });
  }


  setItems(items, showItems, state = {}) {
    this.setState({
      items: items,
      showItems: showItems,
      ...state
    });

    let { filter } = state;
    filter = filter || this.state.filter;

    AsyncStorage.setItem(STATE_KEY, JSON.stringify({ items, filter }));
  }

  handleAddItem() {
    if (!this.state.value) return;

    const newItems = [
      ...this.state.items,
      {
        key: Date.now(),
        text: this.state.value,
        complete: false,
      },
    ];

    this.setItems(newItems, filterItems(newItems, this.state.filter), { value: '' });
  }

  handleToggleAllComplete() {
    const complete = !this.state.allComplete;
    const newItems = this.state.items.map(item => ({
      ...item,
      complete
    }));

    this.setItems(newItems, filterItems(newItems, this.state.filter));
  }

  handleToggleComplete(key, complete) {
    const newItems = this.state.items.map(item => {
      if (item.key !== key) return item;

      return {
        ...item,
        complete
      };
    });

    this.setItems(newItems, filterItems(newItems, this.state.filter));
  }

  handleRemoveItem(key) {
    let items = this.state.items.filter(item => item.key !== key);

    this.setItems(items, filterItems(items, this.state.filter));
  }

  handleFilter(filter) {
    this.setItems(this.state.items, filterItems(this.state.items, filter), { filter });
  }

  renderRow(item) {
    return <Row
      handleToggleComplete={(complete) => this.handleToggleComplete(item.key, complete)}
      handleRemoveItem={() => this.handleRemoveItem(item.key)}
      key={item.key} item={item}
    />;
  }

  handleClearComplete() {
    const newItems = this.state.items.filter(item => !item.complete);

    this.setItems(newItems, filterItems(newItems, this.state.filter));
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          value={this.state.value}
          onAddItem={this.handleAddItem}
          onChange={(value) => this.setState({ value })}
          handleToggleComplete={this.handleToggleAllComplete}
        />
        <View style={styles.content}>
          <SectionList
            style={styles.list}
            renderItem={({ item }) => this.renderRow(item)}
            renderSectionHeader={({ section }) => <Section section={section}/>}
            sections={[
              { title: 'TODOs', data: this.state.showItems }
            ]}
          />
        </View>

        <Footer
          onFilter={this.handleFilter}
          filter={this.state.filter}
          count={filterItems(this.state.items, FILTER_ACTIVE).length}
          onClearComplete={this.handleClearComplete}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    ...Platform.select({
      ios: {
        paddingTop: 30
      }
    })
  },
  content: {
    flex: 1,
  },
  separator: {
    flex: 1,
    backgroundColor: '#444'
  },
  list: {
    backgroundColor: '#CCC',
    flex: 1,
  }
});
